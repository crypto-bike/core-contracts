import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { deployItem } from "./Factory";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Item", () => {
    it('should mint item with proof', async () => {
        const { item, signers, itemType } = await loadFixture(deployItem);

        const proofSignerAddress = await item.getSignerProof();
        const proofSigner = signers.find((s) => s.address === proofSignerAddress);
        if (!proofSigner) throw new Error('Proof signer not found');

        const expire = Date.now();
        const packed = ethers.utils.solidityPack([
            'address',
            'address',
            'uint256',
            'uint256',
        ], [
            signers[0].address,
            signers[0].address,
            itemType,
            expire,
        ]);
        const data = ethers.utils.solidityKeccak256(
            ['bytes'],
            [packed],
        );
        const signature = await proofSigner.signMessage(ethers.utils.arrayify(data));

        await expect(item.mintWithProof({
            caller: signers[0].address,
            to: signers[0].address,
            itemType,
            expire,
            signature,
        }))
            .emit(item, 'Minted')
            .withArgs(itemType, 1, signers[0].address, ethers.constants.AddressZero, 0);
    });

    it('should buy item for stable', async () => {
        const { item, signers, itemType, uniswap } = await loadFixture(deployItem);

        const { priceUsd } = await item.getItemInfo(itemType);
        await uniswap.stableToken.approve(item.address, priceUsd);

        const balanceBefore = await uniswap.stableToken.balanceOf(signers[0].address);
        await expect(item.buyItem(signers[0].address, itemType, uniswap.stableToken.address))
            .emit(item, 'Minted')
            .withArgs(itemType, 1, signers[0].address, uniswap.stableToken.address, priceUsd);
        const balanceAfter = await uniswap.stableToken.balanceOf(signers[0].address);

        expect(balanceBefore.sub(balanceAfter)).eq(priceUsd);
    });

    it('should buy item for token', async () => {
        const { item, signers, itemType, uniswap } = await loadFixture(deployItem);

        const { priceUsd } = await item.getItemInfo(itemType);
        const priceMtcb = priceUsd.mul(2);
        await uniswap.router.setTestRes([priceMtcb]);
        await uniswap.mtcbToken.approve(item.address, priceMtcb);

        const balanceBefore = await uniswap.mtcbToken.balanceOf(signers[0].address);
        await expect(item.buyItem(signers[0].address, itemType, uniswap.mtcbToken.address))
            .emit(item, 'Minted')
            .withArgs(itemType, 1, signers[0].address, uniswap.mtcbToken.address, priceMtcb);
        const balanceAfter = await uniswap.mtcbToken.balanceOf(signers[0].address);

        expect(balanceBefore.sub(balanceAfter)).eq(priceMtcb);
    });

    it('should not buy item if it is not for sale', async () => {
        const { item, itemType, signers, uniswap } = await loadFixture(deployItem);

        await item.setItemInfo(itemType, { priceUsd: 0, forSale: false, exists: true });
        await expect(
            item.buyItem(signers[0].address, itemType, uniswap.stableToken.address)
        ).revertedWith("This item cannot be bought");
    });

    it('should transfer batch', async () => {
        const { item, signers, uniswap } = await loadFixture(deployItem);

        const itemType = 2;
        await item.setItemInfo(itemType, { priceUsd: 0, forSale: true, exists: true });

        await item.buyItem(signers[0].address, itemType, uniswap.stableToken.address);
        await item.buyItem(signers[0].address, itemType, uniswap.stableToken.address);
        await item.buyItem(signers[0].address, itemType, uniswap.stableToken.address);

        await item.setApprovalForAll(signers[2].address, true);
        await item.connect(signers[2])["transferBatchFrom(address,address,uint256[])"](
            signers[0].address, signers[3].address, [1, 2, 3]);
        const balance = await item.balanceOf(signers[3].address);

        expect(balance).eq(3);
    });
});
