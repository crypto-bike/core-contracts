/**
 * Returns an int value [min; max]
 */
export function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Returns an float value [min; max]
 */
export function getRandomFloat(min: number, max: number) {
    return Math.random() * (max - min) + min;
}
