//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "./IERC4907.sol";

interface IERC721MintableUpgradeable is IERC4907 {
    event Minted(
        uint256 itemType,
        uint256 tokenId,
        address to,
        address paymentToken,
        uint price
    );

    struct MintAllowedProof {
        address caller;
        address to;
        uint256 itemType;
        uint256 expire;
        bytes signature;
    }
    
    struct PaymentStrategy {
        IUniswapV2Router02 router;
        address[] path;
        address treasury;
        bool exists;
    }

    struct ItemInfo {
        uint256 priceUsd;
        bool forSale;
        bool exists;
    }

    function getSignerProof() external view returns(address);
    function getItemInfo(
        uint256 itemType
    ) external view returns(uint256 priceUsd, bool exists);
    function getPaymentStrategy(
        address paymentToken
    ) external view returns(PaymentStrategy memory);
    function mintWithProof(MintAllowedProof calldata proof) external;
    function buyItem(
        address to,
        uint256 itemType,
        address paymentToken
    ) external;
    function mintAdmin(
        address to,
        uint256 itemType
    ) external;
}
