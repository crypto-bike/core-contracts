//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "./ERC721MintableUpgradeable.sol";

contract Item is ERC721MintableUpgradeable {
    function initialize(
        string memory _name,
        string memory _symbol,
        string memory _baseTokenURI,
        address _signer
    ) initializer external {
        __initialize(_name, _symbol, _baseTokenURI, _signer);
    }
}
