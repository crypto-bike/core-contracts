//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract FreeItem is ERC721Upgradeable, AccessControlUpgradeable {
    using Counters for Counters.Counter;
    Counters.Counter private counter;
    string private baseTokenURI;

    mapping (address => bool) public isUserMinted;

    function initialize(
        string memory _name,
        string memory _symbol,
        string memory _baseTokenURI
    ) initializer external {
        __ERC721_init_unchained(_name, _symbol);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        baseTokenURI = _baseTokenURI;
    }

    function setBaseTokenURI(
        string memory _baseTokenURI
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseTokenURI = _baseTokenURI;
    }

    function supportsInterface(
        bytes4 interfaceId
    )
        public
        view
        override(ERC721Upgradeable, AccessControlUpgradeable)
        returns (bool)
    {
        return ERC721Upgradeable.supportsInterface(interfaceId)
            || AccessControlUpgradeable.supportsInterface(interfaceId);
    }

    function freeMint() external {
        require(!isUserMinted[msg.sender], "Free mint allowed only once");

        counter.increment();
        uint256 newItemId = counter.current();
        _mint(msg.sender, newItemId);
        isUserMinted[msg.sender] = true;
    }

    function _baseURI()
        internal
        view
        virtual
        override
    returns (string memory) {
        return baseTokenURI;
    }

    function _beforeTokenTransfer(
        address from,
        address,
        uint256
    ) internal virtual override {
        if (from != address(0)) revert("Free item transfer is not allowed");
    }
}
