//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

import "../BaseSignerProof.sol";
import "./interfaces/IERC721MintableUpgradeable.sol";

abstract contract ERC721MintableUpgradeable is
    IERC721MintableUpgradeable,
    BaseSignerProof,
    ERC721EnumerableUpgradeable,
    AccessControlUpgradeable
{
    using Counters for Counters.Counter;
    Counters.Counter private counter;
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    mapping(uint256  => UserInfo) internal users;
    mapping(address => PaymentStrategy) internal paymentMethods;
    mapping(uint256 => ItemInfo) internal itemInfoByType;
    string internal baseTokenURI;

    function __initialize(
        string memory _name,
        string memory _symbol,
        string memory _baseTokenURI,
        address _signer
    ) onlyInitializing internal {
        __ERC721_init(_name, _symbol);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        baseTokenURI = _baseTokenURI;
        signer = _signer;
    }

    function supportsInterface(
        bytes4 interfaceId
    )
        public
        view
        override(ERC721EnumerableUpgradeable, AccessControlUpgradeable)
        returns (bool)
    {
        return ERC721EnumerableUpgradeable.supportsInterface(interfaceId)
            || AccessControlUpgradeable.supportsInterface(interfaceId)
            || interfaceId == type(IERC4907).interfaceId;
    }

    function getSignerProof() external view returns(address) {
        return signer;
    }

    function getItemInfo(
        uint256 itemType
    ) external view returns(uint256 priceUsd, bool exists) {
        ItemInfo memory info = itemInfoByType[itemType];
        priceUsd = info.priceUsd;
        exists = info.exists;
    }

    function getPaymentStrategy(
        address paymentToken
    ) external view returns(PaymentStrategy memory) {
        return paymentMethods[paymentToken];
    }

    function setSigner(
        address _signer
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        signer = _signer;
    }

    function setItemInfo(
        uint256 itemType,
        ItemInfo calldata info
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        itemInfoByType[itemType] = info;
    }

    function setItemInfoBatch(
        uint256[] calldata itemTypes,
        ItemInfo[] calldata infoArray
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(itemTypes.length == infoArray.length);

        for (uint256 i; i < itemTypes.length; i++)
            setItemInfo(itemTypes[i], infoArray[i]);
    }

    function registerPaymentToken(
        address paymentToken,
        PaymentStrategy calldata strategy
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        paymentMethods[paymentToken] = strategy;
    }

    function setBaseTokenURI(
        string memory _baseTokenURI
    ) public onlyRole(DEFAULT_ADMIN_ROLE) {
        baseTokenURI = _baseTokenURI;
    }

    function mintWithProof(MintAllowedProof calldata proof) external {
        useSignature(
            abi.encodePacked(
                proof.caller,
                proof.to,
                proof.itemType,
                proof.expire
            ),
            proof.signature,
            proof.expire
        );
        require(proof.caller == msg.sender, "Invalid account");

        mintItem(proof.to, proof.itemType);
    }

    function buyItem(
        address to,
        uint256 itemType,
        address paymentToken
    ) external {
        require(itemInfoByType[itemType].forSale, "This item cannot be bought");
        uint256 payed = _withdrawPayment(msg.sender, paymentToken, itemInfoByType[itemType].priceUsd);
        mintItem(to, itemType, paymentToken, payed);
    }

    function mintAdmin(
        address to,
        uint256 itemType
    ) external onlyRole(MINTER_ROLE) {
        mintItem(to, itemType, address(0), 0);
    }

    function mintItem(
        address to,
        uint256 itemType
    ) internal {
        mintItem(to, itemType, address(0), 0);
    }

    function mintItem(
        address to,
        uint256 itemType,
        address paymentToken,
        uint256 payed
    ) internal {
        require(itemInfoByType[itemType].exists, "Item is not exist");

        counter.increment();
        uint256 newItemId = counter.current();
        _mint(to, newItemId);

        emit Minted(itemType, newItemId, to, paymentToken, payed);
    }

    function transferBatchFrom(
        address from,
        address to,
        uint256[] calldata ids
    ) external {
        transferBatchFrom(from, to, ids, "");
    }

    function transferBatchFrom(
        address from,
        address to,
        uint256[] calldata ids,
        bytes memory data
    ) public {
        for(uint256 i; i < ids.length; i++)
            transferFrom(from, to, ids[i]);

        if (data.length > 0) {
            (bool success, bytes memory response) = to.call(data);
            if (!success) revert(string(response));
        }
    }

    function setUser(uint256 tokenId, address user, uint64 expires) public {
        require(_isApprovedOrOwner(msg.sender, tokenId), "ERC4907: transfer caller is not owner nor approved");
        UserInfo storage info =  users[tokenId];
        info.user = user;
        info.expires = expires;
        emit UpdateUser(tokenId, user, expires);
    }

    function userOf(uint256 tokenId) public view returns(address) {
        if(uint256(users[tokenId].expires) >=  block.timestamp) {
            return users[tokenId].user;
        } else {
            return address(0);
        }
    }

    function userExpires(uint256 tokenId) public view returns(uint256) {
        return users[tokenId].expires;
    }

    function _withdrawPayment(
        address from,
        address paymentToken,
        uint256 price
    ) internal returns (uint256) {
        if (price == 0) return 0;

        PaymentStrategy memory strategy = paymentMethods[paymentToken];
        require(strategy.exists, "Payment strategy is not exist");
        if (address(strategy.router) != address(0)) {
            price = strategy.router.getAmountsIn(price, strategy.path)[0];
        }

        require(
            IERC20(paymentToken).transferFrom(from, strategy.treasury, price),
            "Transfer failed"
        );
        return price;
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override {
        super._beforeTokenTransfer(from, to, tokenId);

        if (from != to && users[tokenId].user != address(0)) {
            delete users[tokenId];
            emit UpdateUser(tokenId, address(0), 0);
        }
    }
    
    function _baseURI()
        internal
        view
        virtual
        override
    returns (string memory) {
        return baseTokenURI;
    }
}
