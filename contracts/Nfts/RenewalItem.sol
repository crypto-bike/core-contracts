//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "./ERC721MintableUpgradeable.sol";

contract RenewalItem is ERC721MintableUpgradeable {
    mapping(address => bool) public renewalAllowed;

    function initialize(
        string memory _name,
        string memory _symbol,
        string memory _baseTokenURI,
        address _signer
    ) initializer external {
        __initialize(_name, _symbol, _baseTokenURI, _signer);
        itemInfoByType[0] = ItemInfo({ priceUsd: 0, exists: true, forSale: false });
    }

    function setRenewalAllowed(
        address _token,
        bool _allowed
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        renewalAllowed[_token] = _allowed;
    }

    function renewal(
        IERC721Upgradeable _nft,
        uint256 _tokenId
    ) external {
        require(renewalAllowed[address(_nft)], "Only allowed nfts");
        _nft.transferFrom(msg.sender, address(this), _tokenId);
        mintItem(msg.sender, 0);
    }
}
