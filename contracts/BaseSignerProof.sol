//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

contract BaseSignerProof {
    mapping(bytes32 => bool) internal hashes;
    address internal signer;

    function useSignature(
        bytes memory dataHash,
        bytes memory signature,
        uint256 expire
    ) internal {
        bytes32 msgHash = keccak256(
            abi.encodePacked(
                "\x19Ethereum Signed Message:\n32",
                keccak256(dataHash)
            )
        );

        address _signer = ECDSA.recover(msgHash, signature);
        require(!hashes[msgHash], "Signature proof already used");
        require(_signer == signer, "Signer is not valid");
        require(block.timestamp < expire, "Signature expired");

        hashes[msgHash] = true;
    }
}
